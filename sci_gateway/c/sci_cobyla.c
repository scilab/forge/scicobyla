#include <math.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>

#include <stack-c.h>
#include <MALLOC.h>
#include <api_scilab.h>
#include <sciprint.h>
#include <freeArrayOfString.h>
#include <do_error_number.h>

#include "cobyla.h"

#define X_IN         1
#define F_IN         2
#define NB_CONSTR_IN 3
#define RHOBEG_IN    4
#define RHOEND_IN    5
#define MESSAGE_IN   6
#define MAXFUN_IN    7
#define LAST_ARG     7
#define X_OPT_OUT    Rhs+1
#define STATUS_OUT   Rhs+2
#define MAXFUN_OUT   Rhs+3

#define OK   1
#define FAIL 0

//#define DEBUG 1

#define _(A) A
#define MAX(A,B) ((A<B)?B:A)

#define COBYLA_ERROR if(_SciErr.iErr)   \
    {           \
      printError(&_SciErr, 0);      \
      return 0;         \
    }

cobyla_function * objective;

/******************************
 * Work around for the        *
 * AddFunctionInTable problem *
 ******************************/

typedef void (*voidf)();
typedef struct {
  char *name;
  voidf f;
} FTAB;

extern voidf AddFunctionInTable (char *name, int *rep, FTAB *table);

/******************************************
 * General functions for scilab interface *
 ******************************************/

// SearchInDynLinks: a scilab function which tries to find a C/C++/Fortran function loaded via link
// SearchComp: a function which tries to find a C/C++/Fortran function stored in the FTAB table (ours is empty)
// SetFunction: this function looks inside the table or inside Scilab for dynamically loaded functions
// sciobj: a wrapper function which has the same prototype as a dynamically loaded C/C++/Fortran function and which is used
//         to call a Scilab script function like a C/C++/Fortran function
// Emptyfunc: an empty function which is used when no dynamically functions has been found
// GetFunctionPtr: the main function. Get the parameter on the stack. If it's a Scilab function, then it returns a pointer to
//                 sciobj. Otherwise, returns a pointer to the dynamically loaded function

extern int   SearchInDynLinks(char *op, void (**realop) ());
static int   SearchComp(FTAB *Ftab, char *op, void (**realop) ( ));
static voidf SetFunction(char *name, int *rep, FTAB *table);
static int   sciobj(int size_x, int size_constr, double * x, double * f, double * con, void * param);
static void  Emptyfunc(void) {} ;
voidf        GetFunctionPtr(char *, int, FTAB *, voidf, int *, int*, int*);

struct param_fobj {
  int sci_obj;
  int lhs_obj;
  int rhs_obj;
  int stack_pos;
};

/***********************
 *Definition of FTables*
 ***********************/

static FTAB FTab_cobyla_function[] = {{(char *) 0, (voidf) 0}};

/************************
 * The Scilab interface *
 ************************/

static jmp_buf cobyla_function_env;
static int sci_obj, lhs_obj, rhs_obj;

int sci_cobyla(char * fname)
{
  int m_x,        n_x,        * x_addr        = NULL;
  int m_f = 1,    n_f = 1;
  int m_nbconstr, n_nbconstr, * nbconstr_addr = NULL;
  int m_rhobeg,   n_rhobeg,   * rhobeg_addr   = NULL;
  int m_rhoend,   n_rhoend,   * rhoend_addr   = NULL;
  int m_message,  n_message,  * message_addr  = NULL;
  int m_maxfun,   n_maxfun,   * maxfun_addr   = NULL;
  double * pdbl_x = NULL;
  double * pdbl_nbconstr = NULL;
  double * pdbl_rhobeg = NULL;
  double * pdbl_rhoend = NULL;
  double * pdbl_message = NULL;
  double * pdbl_maxfun = NULL;
  int size_x, size_constr, maxfun, message, res;
  struct param_fobj fobj_param;
  SciErr _SciErr;

  CheckRhs(LAST_ARG,LAST_ARG);
  CheckLhs(1,3);

  // Get X
  _SciErr = getVarAddressFromPosition(pvApiCtx, X_IN, &x_addr); COBYLA_ERROR;
  _SciErr = getMatrixOfDouble(pvApiCtx, x_addr, &m_x, &n_x, &pdbl_x); COBYLA_ERROR;
  size_x = m_x*n_x;

  // Get F
  // Here, we get either a Scilab script function or a string which gives the name of the C/C++/Fortran function
  // loaded via "link".
  objective = (cobyla_function *)GetFunctionPtr("cobyla_f", F_IN, FTab_cobyla_function, (voidf)sciobj, &sci_obj, &lhs_obj, &rhs_obj);

  fobj_param.sci_obj   = sci_obj;
  fobj_param.lhs_obj   = lhs_obj;
  fobj_param.rhs_obj   = rhs_obj;
  fobj_param.stack_pos = Rhs+1; // We preallocate the objective function value on position 3 on the stack
                                // So, scifunction will create all the needed variables on Rhs+2 == 4 to
                                // avoid the destruction of this preallocated output variable

  if (objective==(cobyla_function *)0)
    {
      sciprint("%s : Error - Last argument must be a pointer to a scilab function", fname);
      return 0;
    }

  // Nb Constraints
  _SciErr = getVarAddressFromPosition(pvApiCtx, NB_CONSTR_IN, &nbconstr_addr); COBYLA_ERROR;
  _SciErr = getMatrixOfDouble(pvApiCtx, nbconstr_addr, &m_nbconstr, &n_nbconstr, &pdbl_nbconstr); COBYLA_ERROR;
  size_constr = (int)pdbl_nbconstr[0];

  // rhobeg
  _SciErr = getVarAddressFromPosition(pvApiCtx, RHOBEG_IN, &rhobeg_addr); COBYLA_ERROR;
  _SciErr = getMatrixOfDouble(pvApiCtx, rhobeg_addr, &m_rhobeg, &n_rhobeg, &pdbl_rhobeg); COBYLA_ERROR;

  // rhoend
  _SciErr = getVarAddressFromPosition(pvApiCtx, RHOEND_IN, &rhoend_addr); COBYLA_ERROR;
  _SciErr = getMatrixOfDouble(pvApiCtx, rhoend_addr, &m_rhoend, &n_rhoend, &pdbl_rhoend); COBYLA_ERROR;

  // message
  _SciErr = getVarAddressFromPosition(pvApiCtx, MESSAGE_IN, &message_addr); COBYLA_ERROR;
  _SciErr = getMatrixOfDouble(pvApiCtx, message_addr, &m_message, &n_message, &pdbl_message); COBYLA_ERROR;
  message = (int)pdbl_message[0];

  // maxfun
  _SciErr = getVarAddressFromPosition(pvApiCtx, MAXFUN_IN, &maxfun_addr); COBYLA_ERROR;
  _SciErr = getMatrixOfDouble(pvApiCtx, maxfun_addr, &m_maxfun, &n_maxfun, &pdbl_maxfun); COBYLA_ERROR;
  maxfun = (int)pdbl_maxfun[0];

  res = cobyla(size_x, size_constr, pdbl_x, pdbl_rhobeg[0], pdbl_rhoend[0], message, &maxfun, (cobyla_function *)objective, (void *)&fobj_param);

#ifdef DEBUG
  sciprint("size_x = %d res = %d\n", size_x, res);
  for(i=0;i<size_x;i++) sciprint("x_opt[%d] = %f\n", i, pdbl_x[i]);
#endif
  _SciErr = createMatrixOfDouble(pvApiCtx, X_OPT_OUT, size_x, 1, pdbl_x); COBYLA_ERROR;
  if (Lhs>=2)
    {
      _SciErr = allocMatrixOfDouble(pvApiCtx, STATUS_OUT, 1, 1, &pdbl_x); COBYLA_ERROR;
      pdbl_x[0] = (double)res;
    }
  if (Lhs>=3)
    {
      _SciErr = allocMatrixOfDouble(pvApiCtx, MAXFUN_OUT, 1, 1, &pdbl_x); COBYLA_ERROR;
      pdbl_x[0] = (double)maxfun;
    }

  LhsVar(1) = X_OPT_OUT;
  if (Lhs>=2)
    {
      LhsVar(2) = STATUS_OUT;
    }
  if (Lhs>=3)
    {
      LhsVar(3) = MAXFUN_OUT;
    }

  return 0;
}

/**********
 * sciobj *
 **********/

// This function is a wrapper which allows to call a Scilab script function like a "normal"
// C/C++/Fortran function
int sciobj(int size_x, int size_constr, double * x, double * f, double * con, void * param)
{
  int n_x = size_x, m_x = 1;
  int n_tmp, m_tmp, * tmp_addr = NULL;
  int sci_obj, lhs_obj, rhs_obj;
  int stack_pos, i, output_state;
  int rhs_old = Rhs, nbvars_old = Nbvars, Index = 0;
  double * x_sci = NULL;
  double * tmp_var = NULL;
  SciErr _SciErr;

  sci_obj   = ((struct param_fobj *)param)->sci_obj;
  lhs_obj   = ((struct param_fobj *)param)->lhs_obj;
  rhs_obj   = ((struct param_fobj *)param)->rhs_obj;
  stack_pos = ((struct param_fobj *)param)->stack_pos;

  Rhs    = stack_pos + MAX(rhs_obj,lhs_obj);
  Nbvars = stack_pos + MAX(rhs_obj,lhs_obj);

#ifdef DEBUG
  sciprint("DEBUG: lhs = %d rhs = %d stack_pos = %d size_x = %d, size_constr = %d\n", lhs_obj, rhs_obj, stack_pos, size_x, size_constr);
#endif

  _SciErr = allocMatrixOfDouble(pvApiCtx, stack_pos+0, n_x, m_x, &x_sci); COBYLA_ERROR;
  for(i=0;i<size_x;i++)
    {
      x_sci[i] = x[i];
    }

  _SciErr = allocMatrixOfDouble(pvApiCtx, stack_pos+1, 0, 0, &tmp_var);
  _SciErr = allocMatrixOfDouble(pvApiCtx, stack_pos+2, 0, 0, &tmp_var);

  //PExecSciFunction(stack_pos, &sci_obj, &lhs_obj, &rhs_obj, "sciobj", cobyla_function_env);
  SciFunction(&stack_pos, &sci_obj, &lhs_obj, &rhs_obj);

  _SciErr = getVarAddressFromPosition(pvApiCtx, stack_pos+0, &tmp_addr); COBYLA_ERROR;
  _SciErr = getMatrixOfDouble(pvApiCtx, tmp_addr, &n_tmp, &m_tmp, &tmp_var); COBYLA_ERROR;
  f[0] = tmp_var[0];

  _SciErr = getVarAddressFromPosition(pvApiCtx, stack_pos+1, &tmp_addr); COBYLA_ERROR;
  _SciErr = getMatrixOfDouble(pvApiCtx, tmp_addr, &n_tmp, &m_tmp, &tmp_var); COBYLA_ERROR;
  // YC: check size: n_tmp*m_tmp == size_constr
  if (size_constr!=0)
    {
      for(i=0;i<size_constr;i++) con[i] = tmp_var[i];
    }

  _SciErr = getVarAddressFromPosition(pvApiCtx, stack_pos+2, &tmp_addr); COBYLA_ERROR;
  _SciErr = getMatrixOfDouble(pvApiCtx, tmp_addr, &n_tmp, &m_tmp, &tmp_var); COBYLA_ERROR;
  output_state = (int)tmp_var[0];

#ifdef DEBUG
  sciprint("DEBUG: f = %f, tmp_val = %f size_x = %d, sizeconstr = %d\n", f[0], tmp_val, size_x, size_constr);
#endif

  Rhs    = rhs_old;
  Nbvars = nbvars_old;

  // COBYLA_MINRC     = -2 - Constant to add to get the rc_string
  // COBYLA_EINVAL    = -2 - N<0 or M<0
  // COBYLA_ENOMEM    = -1 - Memory allocation failed
  // COBYLA_NORMAL    =  0 - Normal return from cobyla
  // COBYLA_MAXFUN    =  1 - Maximum number of function evaluations reach
  // COBYLA_ROUNDING  =  2 - Rounding errors are becoming damaging
  // COBYLA_USERABORT =  3 - User requested end of minimization

  // YC: check values

  return output_state;
}

// This function returns a pointer to a function or a Scilab pointer to a function
// The choice is performed on the type of the given Scilab parameter
voidf GetFunctionPtr(char * name, int n, FTAB Table[], voidf scifun, int * ifunc, int * lhs, int * rhs)
{
  int type, rep, m_tmp, n_tmp, i;
  int * tmp_addr = NULL;
  char * tmp_char = NULL;
  int * pi_len = NULL;
  char ** pst_strings = NULL;
  voidf f;
  SciErr _SciErr;

  _SciErr = getVarAddressFromPosition(pvApiCtx, n, &tmp_addr);
  _SciErr = getVarType(pvApiCtx, tmp_addr, &type);

  switch(type)
    {
    case sci_strings:
      _SciErr = getMatrixOfString(pvApiCtx, tmp_addr, &n_tmp, &m_tmp, NULL, NULL);
      pi_len = (int *)MALLOC(n_tmp*m_tmp*sizeof(int));
      pst_strings = (char **)MALLOC(n_tmp*m_tmp*sizeof(char *));
      _SciErr = getMatrixOfString(pvApiCtx, tmp_addr, &n_tmp, &m_tmp, pi_len, NULL);
      for(i=0;i<n_tmp*m_tmp;i++) pst_strings[i] = (char *)MALLOC((pi_len[i]+1)*sizeof(char));
      _SciErr = getMatrixOfString(pvApiCtx, tmp_addr, &n_tmp, &m_tmp, pi_len, pst_strings);

      f = SetFunction(pst_strings[0], &rep, Table);

      if (pst_strings) freeArrayOfString(pst_strings, n_tmp*m_tmp);
      if (pi_len) FREE(pi_len);

      if (rep==1)
  {
    SciError(999);
    return (voidf)0;
  }

      return f ;

    case sci_c_function:
      GetRhsVar(n, "f", lhs, rhs, ifunc);
      return (voidf)scifun ;

    default:
      sciprint("Wrong parameter in %s ! (number %d)\r\n",name,n);
      SciError(999);
      return (voidf)0;
    }
}

// This function searches in the FTAB or in Scilab for corresponding function
voidf SetFunction(char * name, int * rep, FTAB table[])
{
  voidf loc;
  char * s = NULL;
  char buf[csiz];

  strncpy(buf,name,csiz);
  s = buf;
  while((*s!=' ')&&(*s != '\0')) {s++;};
  *s =  '\0';

  if (SearchComp(table,buf,&loc)==OK)
    {
      *rep = 0;
      return(loc);
    }

  if (SearchInDynLinks(buf,&loc)>=0)
    {
      *rep = 0;
      return(loc);
    }

  loc = Emptyfunc;
  *rep = 1;

  return(loc);
}

// This function search in FTAB (here, we will use FTab_cobyla_function) for the corresponding name of the function
int SearchComp(FTAB Ftab[], char * op, void (**realop)())
{
  int i=0;

  while(Ftab[i].name!=(char *)0)
    {
      int j;

      j = strcmp(op,Ftab[i].name);
      if ( j == 0 )
  {
    *realop = Ftab[i].f;
    return(OK);
  }
      else
  {
    if ( j <= 0)
      {
        return(FAIL);
      }
    else i++;
  }
    }

  return(FAIL);
}
