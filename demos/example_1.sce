// =============================================================================
// Test problem 1 (Simple quadratic)
// =============================================================================
function example_1()
nb_constr_test_1 = 0;
xopt_test_1 = [-1 0]';

function [f, con, info] = test_1(x)
  d__1 = x(1) + 1.;
  d__2 = x(2);
  f = d__1 * d__1 * 10. + d__2 * d__2;
  con = 0;
  info = 0;
endfunction

//
// Start COBYLA optimization
//

rhobeg        = 1;
rhoend        = 1e-3;
message_in    = 3;
eval_func_max = 200;

x0 = ones(xopt_test_1);
[x_opt, status, eval_func] = cobyla(x0, test_1, nb_constr_test_1, rhobeg, rhoend, message_in, eval_func_max);

printf("Minimization of a simple quadratic function of two variables.\n");
printf("Iterations needed to solve the problem: %d - status = %d\n", eval_func, status);
printf("solution found:"); disp(x_opt);
printf("comparison to the best know solution: %f\n", norm(x_opt - xopt_test_1));
endfunction
// =============================================================================
example_1();
clear example_1;
// =============================================================================
