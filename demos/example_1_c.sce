// =============================================================================
// External function written in C (C compiler required)
// write down the C code (Simple Quadratic problem)
// =============================================================================
function example_1_c()

nb_constr_test_1 = 0;
xopt_test_1 = [-1 0]';

C=['#include <math.h>'
   'int simple_quadrat(int n, int m, double *x, double *f, double *con, void *state_)'
   '{'
   '  double d__1, d__2;'
   '  d__1 = x[0] + 1.;'
   '  d__2 = x[1];'
   '  *f = d__1 * d__1 * 10. + d__2 * d__2;'
   '  return 0;'
   '}'];

cd TMPDIR;
mputl(C,TMPDIR+'/simple_quadrat.c');

// compile the C code
l=ilib_for_link('simple_quadrat','simple_quadrat.c',[],'c');

// incremental linking
link(l,'simple_quadrat','c');

//
// Start COBYLA optimization
//

rhobeg        = 1;
rhoend        = 1e-3;
message_in    = 3;
eval_func_max = 200;

x0 = ones(xopt_test_1);
[x_opt, status, eval_func] = cobyla(x0, 'simple_quadrat', nb_constr_test_1, rhobeg, rhoend, message_in, eval_func_max);

printf("Minimization of a simple quadratic function of two variables.\n");
printf("Iterations needed to solve the problem: %d - status = %d\n", eval_func, status);
printf("solution found:"); disp(x_opt);
printf("comparison to the best know solution: %f\n", norm(x_opt - xopt_test_1));
endfunction
// =============================================================================
example_1_c()
clear example_1_c;
// =============================================================================
