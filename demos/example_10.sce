// =============================================================================
// Test problem 10 (Hexagon area)
// =============================================================================

function example_10()

nb_constr_test_10 = 14;
xopt_test_10 = ones(9,1);

function [f, con, info] = test_10(x)
  f = (x(1) * x(4) - x(2) * x(3) + x(3) * x(9) - x(5) * x(9) + x(5) * x(8) - x(6) * x(7)) * -.5;
  d__1 = x(3);
  d__2 = x(4);
  con(1) = 1. - d__1 * d__1 - d__2 * d__2;
  d__1 = x(9);
  con(2) = 1. - d__1 * d__1;
  d__1 = x(5);
  d__2 = x(6);
  con(3) = 1. - d__1 * d__1 - d__2 * d__2;
  d__1 = x(1);
  d__2 = x(2) - x(9);
  con(4) = 1. - d__1 * d__1 - d__2 * d__2;
  d__1 = x(1) - x(5);
  d__2 = x(2) - x(6);
  con(5) = 1. - d__1 * d__1 - d__2 * d__2;
  d__1 = x(1) - x(7);
  d__2 = x(2) - x(8);
  con(6) = 1. - d__1 * d__1 - d__2 * d__2;
  d__1 = x(3) - x(5);
  d__2 = x(4) - x(6);
  con(7) = 1. - d__1 * d__1 - d__2 * d__2;
  d__1 = x(3) - x(7);
  d__2 = x(4) - x(8);
  con(8) = 1. - d__1 * d__1 - d__2 * d__2;
  d__1 = x(7);
  d__2 = x(8) - x(9);
  con(9) = 1. - d__1 * d__1 - d__2 * d__2;
  con(10) = x(1) * x(4) - x(2) * x(3);
  con(11) = x(3) * x(9);
  con(12) = -x(5) * x(9);
  con(13) = x(5) * x(8) - x(6) * x(7);
  con(14) = x(9);
  info = 0;
endfunction

//
// Start COBYLA optimization
//

rhobeg        = 1;
rhoend        = 1e-3;
message_in    = 3;
eval_func_max = 200;

x0 = ones(xopt_test_10);
[x_opt, status, eval_func] = cobyla(x0, test_10, nb_constr_test_10, rhobeg, rhoend, message_in, eval_func_max);

printf("This problem is taken from page 415 of Luenberger''s book Applied\n");
printf("Nonlinear Programming. It is to maximize the area of a hexagon of\n");
printf("unit diameter.\n");
printf("Iterations needed to solve the problem: %d - status = %d\n", eval_func, status);
printf("solution found:"); disp(x_opt);
printf("comparison to the best know solution: %f\n", norm(x_opt - xopt_test_10));
endfunction
// =============================================================================
example_10();
clear example_10;
// =============================================================================
